/*
Filename: generator.js
Function: generate element properties from sequence and assign them to elements and write them to the dom.
*/

var cl = console.log;

		// fn : calculate max value from array
function max(array) {
			return array.reduce((a, b) => {
			  return a >= b ? a : b;
			}, 0);
	}
		
		// fn : coerce value into "px" value
		function px(value) {
			return value + "px";
		}
		
		// fn : create wrapper element
		function wrap(width, height) {
			var wrap = document.createElement('div');
				wrap.style.position = "relative";
				wrap.style.display = "block";
				wrap.style.height = px(height);
				wrap.style.width = px(width);
				wrap.style.float = "left";
				wrap.style.margin = "10px";
				wrap.style.border = "1px solid rgba(0,0,0,0.5)";
			return wrap;
		}
		// default line markup
  var defaultLine = document.createElement('div');
defaultLine.style.backgroundColor = "rgba(0,0,0,0.5)";
defaultLine.style.position = "absolute";

		// fn: append dom element to another
		function draw(element, child) {
			element.appendChild(child);
		}
  // what does this thing do?
  	// generate line properties from sequence
		function visualize(sequence) {
			var patterns = [];
			var lines = [];
			var spawnPoint = { // todo calc spawnpoint
				left: 90,
				top: 90
			};

			for(var i = 0; i < sequence.length; i++) {
				var line = defaultLine.cloneNode(true);
				var size = 4;
				var o = sequence[i]; // operator/line
   var prev = sequence[i-1];
   var next = sequence[i+1];
   var history = [];
   var future = [];
   var d = "direction", v = "value";
				var x = "left", y = "top";
				// for debugging purposes
				line.textContent = o.direction + o.value;
				line.style.fontSize = px(8);
				line.style.color = "rgba(13, 200, 130, 1.0)";
				switch(o.direction) {
					case "down":
					 // smell, should create handler fn..`
					 // z-index, colors,..
					 // assign physical line properties
					 line.style.height = px(o.value);
					 line.style.width = px(size);
					 // set spawn point
					 	if(typeof prev === "undefined"){
					 		// should be exact conditional
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);  
					 	}else if(prev[d] === "right"){
					 	  // calculate spawnpoint
					 	  spawnPoint[y];
					 	  spawnPoint[x] += prev[v];
					 	  // assign spawnpoint properties
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "left"){
					 	  // calculate spawnpoint
					 	  spawnPoint[y] += size;
					 	  spawnPoint[x];
					 	  // assign spawnpoint properties
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "down"){ 
  			 	  // calculate spawnpoint
					 	  spawnPoint[y] += prev[v];
					 	  spawnPoint[x];
					 	  // assign spawnpoint properties
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "up"){
       spawnPoint[y];
					 	  spawnPoint[x];
					 	  // assign spawnpoint properties
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }
					break;
					
					case "right":
					 line.style.height = px(size);
					 line.style.width = px(o.value);
					 	if(typeof prev === "undefined"){
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);  
					 	}else if(prev[d] === "right"){ // should it be defined? yeah could apply colors..
					 	  spawnPoint[y];
					 	  spawnPoint[x] -= o.value;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "left"){
					 	  spawnPoint[y];
					 	  spawnPoint[x];
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "down"){ 
					 	  spawnPoint[y] += prev[v];
					 	  spawnPoint[x];
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "up"){
					 	  spawnPoint[y];
					 	  spawnPoint[x] += size;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }
					break;
					
					case "up":
					 line.style.height = px(o.value);
					 line.style.width = px(size);
					 	if(typeof prev === "undefined"){
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);  
					 	}else if(prev[d] === "right"){
					 	  spawnPoint[y] -= o.value;
					 	  spawnPoint[x] += prev[v] - size;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "left"){
					 	  spawnPoint[y] -= o.value;
					 	  spawnPoint[x];
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "down"){ 
					 	  spawnPoint[y];
					 	  spawnPoint[x];
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "up"){
					 	  spawnPoint[y] -= o.value;
					 	  spawnPoint[x];
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }
					break;
					
					case "left":
					 line.style.height = px(size);
					 line.style.width = px(o.value);
					 	if(typeof prev === "undefined"){
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);  
					 	}else if(prev[d] === "right"){
					 	  spawnPoint[y];
					 	  spawnPoint[x] += prev[v] - o.value;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "left"){
					 	  spawnPoint[y];
					 	  spawnPoint[x] += prev[v] - o.value;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "down"){ 
					 	  spawnPoint[y] += prev[v] - size;
					 	  spawnPoint[x] -= o.value;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }else if(prev[d] === "up"){
					 	  spawnPoint[y];
					 	  spawnPoint[x] -= o.value;
					 	  line.style[y] = px(spawnPoint[y]);
					 	  line.style[x] = px(spawnPoint[x]);
					 }
					break;
				}
				lines.push(line);
			}
			return lines;
		};
		
  // fn: action of visualising and drawing
		function patternGenerator(element, sequence, speed = 200) {
			var visual = visualize(sequence);
  cl(visual);
			for(var i = 0; i < sequence.length; i++) {
				setTimeout((function(x) {
		        	return function() {
		        		draw(element, visual[x]);
		        	};
		    	})(i), speed * i);
			}			
}
		
 permutation(input).filter(sequence => {
  return sequenceValid(sequence.map(obj => obj.direction));
 })
 /*[[
 {direction: "down", value: 100},
 {direction: "right", value: 50},
 {direction: "up", value: 50},
 {direction: "left", value: 70},
 {direction: "up", value: 30},
 {direction: "right", value: 280},
 {direction: "down", value: 80},
 {direction: "left", value: 10}
 */.forEach(seq => {
			var wrapper = wrap(200, 200);
			wrapper.style.fontSize = px(6);
			wrapper.textContent = JSON.stringify(seq.map(p => p.direction + "[" + p.value + "]"));
			draw(document.body, wrapper);
			patternGenerator(wrapper, seq, 50);
		});