	var cl = console.log;
	
	function isPossible(a, b){
			 var r = true;
			 var lr = ["right", "left"];
			 var ud = ["up", "down"];
			 var impossible = [lr, ud];
			 
			 impossible.forEach(imp => {
		   if(imp.indexOf(a) !== -1 &&
		    imp.indexOf(b) !== -1){
		    	 r = false;
		    	}
		   });
		   return r;
		}
		// random balancing applied :)
		function sequenceValid(seq){
			var r = true;
			var random = Math.random();
			cl(0.3, random)
			if(random > 0.30){
				cl(1);
			 r = !seq.map((d, idx)=> {
			  return isPossible(d, seq[idx+1]);
		  }).includes(false);
			}else{
				cl(2);
			 seq.forEach((d, idx)=> {
			   if(isPossible(d, seq[idx+1]) === false){
			   	r = false;
			   }
		  });
			}
			return	r;
		}
		var invalid = ["right", "right", "up", "down"];
		var valid = ["right", "down", "left", "down"];
	
		// cl(isPossible("right", "left"));
		cl("valid", sequenceValid(valid));
		cl("invalid", sequenceValid(invalid));
	
	// ccool now remember the Helper function,
	// and never forget! :p also notice that the
	// order of declaring does not matter here,	
	// only with expressions i believe.
	function Rectangle(arg){
		var rect = this;
		cl(arguments);
		rect.arguments = helper(arg);
		function helper(arg){
			}
		}
Rectangle(1,2,3);
	/*	
	module.exports = {
			sequenceValid,
			invalid,
			valid
			};
			
	*/
	
		function max(array) {
			return array.reduce((a, b) => {
			  return a >= b ? a : b;
			}, 0);
		}