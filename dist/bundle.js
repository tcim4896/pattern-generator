

/*
  patterns.js
  generate pattern sequences
*/

var cl = console.log;

function matches(arr){
	var matches = [];
	for(var i=0, j = 1; i < arr.length; i++, j++){
			var left = arr.slice(i,j);
			var right = arr.slice(j);

		for(var x=0; x < right.length; x++){
		  matches.push(Array(left[0], right[x]));
		}
	}
  return matches;
}

// fn : generate all possible combinations of arr
// i : [['a', 'b'], ['c', 'z'], ['d', 'e', 'f']]
// o : Array(12) [ "acd", "bcd", ...
// plan : get all orders of length n

 var allArrays = [['a', 'b'], ['c', 'z'], ['d', 'e', 'f']];

 function allPossibleCases(arr) {
  if (arr.length == 1) {
    return arr[0];
  } else {
    var result = [];
    var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
    for (var i = 0; i < allCasesOfRest.length; i++) {
      for (var j = 0; j < arr[0].length; j++) {
        result.push(arr[0][j] + allCasesOfRest[i]);
      }
    }
    return result;
  }
}

var r=allPossibleCases(allArrays);
 //outputs ["acd", "bcd", "azd", "bzd", "ace", "bce", "aze", "bze", "acf", 

var ar1 = [1, 2, 3, 4 , 5];

function permutation(array) {
    function p(array, temp) {
        var i, x;
        if (!array.length) {
            result.push(temp);
        }
        for (i = 0; i < array.length; i++) {
            x = array.splice(i, 1)[0];
            p(array, temp.concat(x));
            array.splice(i, 0, x);
        }
    }

    var result = [];
    p(array, []);
    return result;
}

var dir = ["right", "left", "up", "down"];

var dist = [10, 20, 30, 40];

var pat = dir.map((dir, idx) => {
	 return {direction: dir, value: dist[idx]};
})

/*cl(permutation(pat))*//*
  generator.js
   drawing line patterns in the dom
*/
var lineThickness = 4;
var defaultLine = document.createElement('div');
			defaultLine.style.backgroundColor = "#000";
			defaultLine.style.position = "absolute";

		// fn : calculate with "px" string values
		function pxCalc(a, b, mode) {
			var o;
			switch(mode) {
				case 0:
					o = (parseInt(a) + parseInt(b)) + "px";
					break;
				case -1:
					o = (parseInt(a) - parseInt(b)) + "px";
					break;
				default:
					o = (parseInt(a) + parseInt(b)) + "px";
					break;

			}
			return o; 
		}

		// fn : calculate max value from array
		function max(prop, array) {
			return array[0][prop];
		}

		// fn : coerce into "px" string value
		function px(value) {
			return value + "px";
		}
		
		// fn : wrap border around child element with specific size
		function wrap(measurements) {
			var wrap = document.createElement('div');
				wrap.style.position = "relative";
				wrap.style.display = "block";
				wrap.style.height = px(measurements.height + 10);
				wrap.style.width = px(measurements.width + 10);
				wrap.style.float = "left";
				wrap.style.margin = "10px";
				wrap.style.border = "1px solid rgba(0,0,0,0.5)";
			return wrap;
		}

		// fn: add element to the dom (body in this case) #91
		function draw(element, child) {
			element.appendChild(child);
		}

		function visualize(sequence) {
			var patterns = [];
			var lines = [];
			var start = {
				left: 0,
				top: 0
			};

			for(var i = 0; i < sequence.length; i++) {
				var line = defaultLine.cloneNode(true);
				var o = sequence[i];
   var prev = sequence[i-1];
   var next = sequence[i+1];
   var history = [];
   var future = [];
				// set properties derived from sequence-data to element
				
					switch(o.direction) {

					case 'down':
					// cl(start);
						line.style.height = px(o.value);
						line.style.top = start.top;
						start.top = pxCalc(start.top, o.value);
						line.style.width = px(lineThickness);
						line.style.left = start.left;
					break;
					case 'right': 
					// cl(start);
						line.style.height = px(lineThickness);
						line.style.top = start.top;
						start.top = pxCalc(start.top, lineThickness);
						line.style.width = px(o.value);
						line.style.left = start.left;
						start.left = pxCalc(start.left, o.value);

					break;
					case 'up':
					// cl(start);
						line.style.height = px(o.value);
						start.top = pxCalc(start.top, o.value, -1);
						line.style.top = start.top;
						line.style.width = px(lineThickness);
						line.style.left = start.left;
						start.left = pxCalc(start.left, o.value + 10, -1); // huh?
					break;
					case 'left':
					// cl(start);
						line.style.height = px(lineThickness);
						line.style.top = start.top;
						start.top = pxCalc(start.top, lineThickness); // -1??
						line.style.width = px(o.value);
						line.style.left = start.left;
						start.left = pxCalc(start.left, o.value, -1);
					break;

				}

				lines.push(line);
			}

			return lines;
		};

var settings = { 
 speed: 200 
 };

		function patternGenerator(element, sequence, settings) {
			var visual = visualize(sequence);
			// pseudo
			// draw a wrap to body
			// append lines to wrap every pattern

			for(var i = 0; i < sequence.length; i++) {
				setTimeout((function(x) {
		        	return function() {
		        		draw(element, visual[x]);
		        	};
		    	})(i), settings.speed * i);
			}			
}
		
 permutation(pat).forEach(pattern => {
			var measurements = { 
				height: 50,
				width: 50
			};
			var wrapper = wrap(measurements);
			draw(document.body, wrapper);
			patternGenerator(wrapper, pattern, settings);
		});